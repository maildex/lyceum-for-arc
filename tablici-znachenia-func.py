import math


class ReturnSelf:
    def __call__(self, q):
        return q


class SQRT:
    def __call__(self, x):
        return math.sqrt(x)


dict_ = {'x': ReturnSelf(), 'sqrt_fun': SQRT()}


class A:
    def __init__(self, operation, f1, f2):
        self.operation = operation
        self.f1 = f1
        self.f2 = f2

    def __call__(self, arg):
        if self.f1 in dict_:
            a = dict_[self.f1](arg)
        else:
            a = int(self.f1)
        b = dict_[self.f2](arg)
        if self.operation == '+':
            return a + b
        if self.operation == '-':
            return a - b
        if self.operation == '*':
            return a * b
        if self.operation == '/':
            return a / b
        if self.operation == '//':
            return a // b


n = int(input())
for i in range(n):
    s = input().split()
    if s[0] == 'define':
        dict_[s[1]] = A(s[3], s[2], s[4])
    elif s[0] == 'calculate':
        g = s[2:]
        g = [int(j) for j in g]
        w = dict_[s[1]]
        for u in g:
            print(w(u), end=' ')
