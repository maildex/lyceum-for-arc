class Numbers:
    def __init__(self, list):
        self.list = [int(i) for i in list]

    def set_func(self, cmd, print_or_not):
        if cmd == "make_negative":
            for index, i in enumerate(self.list):
                if i > 0:
                    self.list[index] = -i
        if cmd == "square":
            self.list = [i ** 2 for i in self.list]
        if cmd == "strange_command":
            for index, i in enumerate(self.list):
                if i % 5 == 0:
                    self.list[index] += 1

    def get_list(self):
        return self.list


if __name__ == "__main__":
    nums = Numbers(input().split())
    n = int(input())
    for i in range(n):
        if i + 1 == n:
            nums.set_func(input(), print_or_not=True)
        else:
            nums.set_func(input(), print_or_not=False)
    print(*nums.get_list())