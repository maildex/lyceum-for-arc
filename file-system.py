root = {}


class File:
    def __init__(self):
        self.data = {}

    def write_file(self, name, text):
        if name in list(self.data.keys()):
            self.data[name] = text
        else:
            path = name.split('/')
            root[path[-2]].append(path[-1])
            self.data[name] = text

    def read_file(self, name):
        print(self.data[name])


class FileSystem:
    @staticmethod
    def mkdir(name):
        path = name.split('/')
        while len(path) != 1:
            if path[0] in list(root.keys()):
                root[path[0]].append(path[1])
            else:
                root[path[0]] = [path[1]]
            del path[0]
        root[path[0]] = []

    @staticmethod
    def list_dir(name):
        print(root[name])